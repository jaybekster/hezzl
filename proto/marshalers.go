package protos

import "encoding/json"

func (i ListUsersResponse) MarshalBinary() ([]byte, error) {
	return json.Marshal(i)
}
