package main

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	protos "hezzl/proto"
)

func main() {
	conn, err := grpc.Dial("127.0.0.1:12345", grpc.WithInsecure())

	if err != nil {
		logrus.Fatal(err)
	}
	client := protos.NewUserServiceClient(conn)

	addUser(client, &protos.AddUserRequest{Name: "Имя раз"})
	addUser(client, &protos.AddUserRequest{Name: "Имя два"})
	addUser(client, &protos.AddUserRequest{Name: "Имя три"})
	addUser(client, &protos.AddUserRequest{Name: "Имя четыре"})

	deleteUser(client, &protos.DeleteUserRequest{Id: 2})

	ListUsers(client, &protos.ListUsersRequest{})
}

func addUser(client protos.UserServiceClient, req *protos.AddUserRequest) {
	logrus.Infof("Request `%v`", req)

	rsp, err := client.AddUser(context.Background(), req)

	if err != nil {
		logrus.Errorf("server responded with error: `%v`", err)
	} else {
		logrus.Infof("Response: `%v`", rsp)
	}
}

func deleteUser(client protos.UserServiceClient, req *protos.DeleteUserRequest) {
	logrus.Infof("Request `%v`", req)

	rsp, err := client.DeleteUser(context.Background(), req)

	if err != nil {
		logrus.Errorf("server responded with error: `%v`", err)
	} else {
		logrus.Infof("Response: `%v`", rsp)
	}
}

func ListUsers(client protos.UserServiceClient, req *protos.ListUsersRequest) {
	logrus.Infof("Request `%v`")

	rsp, err := client.ListUsers(context.Background(), req)

	if err != nil {
		logrus.Errorf("server responded with error: `%v`", err)
	} else {
		logrus.Infof("Response: `%v`", rsp)
	}
}
