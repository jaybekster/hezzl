package main

import (
	"context"
	"hezzl/lib/kafka"
	"hezzl/lib/postgres"
	"hezzl/lib/redis"
	protos "hezzl/proto"
	"log"
	"net"
	"time"

	_ "github.com/lib/pq"

	"google.golang.org/grpc"
)

type HezzlServer struct {
	protos.UnimplementedUserServiceServer
	db    *postgres.PostgresClient
	redis *redis.RedisClient
	kafka *kafka.KafkaClient
}

func (s *HezzlServer) AddUser(ctx context.Context, req *protos.AddUserRequest) (*protos.User, error) {
	id, err := s.db.AddUser(postgres.DBUser{
		Name: req.Name,
	})

	if err != nil {
		log.Fatal(err)
	}

	err = s.kafka.SendUser(postgres.DBUser{
		Id:   id,
		Name: req.Name,
	})

	if err != nil {
		log.Fatal(err)
	}

	// todo: type casting
	return &protos.User{
		Id:   id,
		Name: req.Name,
	}, nil
}

func (s *HezzlServer) ListUsers(ctx context.Context, req *protos.ListUsersRequest) (*protos.ListUsersResponse, error) {
	cachedValue, err := s.redis.GetUsers()

	if cachedValue != nil && err == nil {
		return cachedValue, nil
	}

	users := make([]*protos.User, 0)

	dbUsers, err := s.db.ListUsers(ctx)

	if err != nil {
		log.Fatal(err)
	}

	// todo: create somehow type casting for a whole slice
	for _, dbUser := range dbUsers {
		users = append(users, &protos.User{
			Id:   dbUser.Id,
			Name: dbUser.Name,
		})
	}

	response := protos.ListUsersResponse{
		Users: users,
	}

	s.redis.SetUsers(response).Result()

	return &response, nil
}

func (s *HezzlServer) DeleteUser(ctx context.Context, req *protos.DeleteUserRequest) (*protos.DeleteUserResponse, error) {
	s.db.DeleteUser(req.Id)

	return &protos.DeleteUserResponse{}, nil
}

func main() {
	lis, err := net.Listen("tcp", "127.0.0.1:12345")

	if err != nil {
		log.Fatalf("failed to listen %v", err)
	}

	postgresClient, err := postgres.New(&postgres.Config{
		Host:     "localhost",
		Port:     5432,
		User:     "hezzl",
		Password: "password",
		DBname:   "hezzl",
	})

	if err != nil {
		log.Fatalf("failed to connect to postgresql %v", err)
	}

	defer postgresClient.Close()

	redisClient, err := redis.New(&redis.Config{
		Addr:       "localhost:6379",
		Password:   "",
		Expiration: time.Minute,
	})

	if err != nil {
		log.Fatalf("failed to connect to redis %v", err)
	}

	kafkaClient, err := kafka.New()

	if err != nil {
		log.Fatalf("failed to connect to kafka %v", err)
	}

	grpcServer := grpc.NewServer()
	hezzlServer := &HezzlServer{
		db:    postgresClient,
		redis: redisClient,
		kafka: kafkaClient,
	}

	protos.RegisterUserServiceServer(grpcServer, hezzlServer)
	grpcServer.Serve(lis)
}
