package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"net"

	_ "github.com/lib/pq"
)

type DBUser struct {
	Id   int64
	Name string
}

type Config struct {
	Host     string
	Port     int
	User     string
	Password string
	DBname   string
}

type PostgresClient struct {
	client *sql.DB
}

func New(config *Config) (*PostgresClient, error) {
	_, _, err := net.SplitHostPort("127.0.0.1:5432")
	if err != nil {
		return nil, err
	}

	connectionString := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Host,
		config.Port,
		config.User,
		config.Password,
		config.DBname,
	)

	db, err := sql.Open("postgres", connectionString)

	if err != nil {
		return nil, err
	}

	return &PostgresClient{
		db,
	}, nil
}

func (postgresClient PostgresClient) AddUser(user DBUser) (int64, error) {
	insertDynStmt := `INSERT INTO "users"("name") values($1) RETURNING id`

	stmt, err := postgresClient.client.Prepare(insertDynStmt)

	if err != nil {
		return -1, err
	}

	var id int64

	err = stmt.QueryRow(user.Name).Scan(&id)

	if err != nil {
		return -1, err
	}

	return id, nil
}

func (postgresClient PostgresClient) ListUsers(ctx context.Context) ([]DBUser, error) {
	// todo: count number in db to allocate for a whole slice or create a stream somehow
	users := make([]DBUser, 0)

	query := `SELECT id, name FROM users`

	rows, err := postgresClient.client.QueryContext(ctx, query)

	if err != nil {
		return []DBUser{}, nil
	}

	defer rows.Close()

	for rows.Next() {
		var id int64
		var name string

		if err := rows.Scan(&id, &name); err != nil {
			fmt.Println(err)
			continue
		}

		user := DBUser{
			Id:   id,
			Name: name,
		}

		users = append(users, user)
	}

	return users, nil
}

func (postgresClient PostgresClient) DeleteUser(id int64) error {
	sqlStatement := `DELETE FROM users WHERE id = $1;`

	res, err := postgresClient.client.Exec(sqlStatement, id)

	if err != nil {
		return err
	}

	_, err = res.RowsAffected()

	if err != nil {
		return err
	}

	return nil
}

func (postgresClient PostgresClient) Close() {
	postgresClient.client.Close()
}
