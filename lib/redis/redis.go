package redis

import (
	"encoding/json"
	protos "hezzl/proto"
	"time"

	"github.com/go-redis/redis"
)

type RedisClient struct {
	client     *redis.Client
	expiration time.Duration
}

type Config struct {
	Addr       string
	Password   string
	Expiration time.Duration
}

func New(config *Config) (*RedisClient, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     config.Addr,
		Password: config.Password,
		DB:       0,
	})

	_, err := client.Ping().Result()

	if err != nil {
		return nil, err
	}

	return &RedisClient{
		client:     client,
		expiration: config.Expiration,
	}, nil
}

func (redisClient RedisClient) SetUsers(value protos.ListUsersResponse) *redis.StatusCmd {
	return redisClient.client.Set("users", value, redisClient.expiration)
}

func (redisClient RedisClient) GetUsers() (*protos.ListUsersResponse, error) {
	val, err := redisClient.client.Get("users").Result()

	if err == redis.Nil {
		return nil, nil
	} else if err != nil {
		return nil, err
	} else {
		if val == "" {
			return nil, nil
		}

		data := protos.ListUsersResponse{}

		json.Unmarshal([]byte(val), &data)

		return &data, nil
	}
}
