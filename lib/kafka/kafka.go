package kafka

import (
	"context"
	"encoding/json"
	"hezzl/lib/postgres"

	"github.com/segmentio/kafka-go"
)

type KafkaClient struct {
	conn *kafka.Conn
}

func New() (*KafkaClient, error) {
	topic := "adding_user"
	partition := 0

	conn, err := kafka.DialLeader(context.Background(), "tcp", "localhost:9092", topic, partition)
	if err != nil {
		return nil, err
	}

	return &KafkaClient{
		conn: conn,
	}, nil
}

func (k KafkaClient) SendUser(user postgres.DBUser) error {
	mappedData, err := json.Marshal(user)

	if err != nil {
		return err
	}

	_, err = k.conn.WriteMessages(kafka.Message{
		Value: []byte(mappedData),
	})

	return err
}

func (k KafkaClient) Close() error {
	err := k.conn.Close()

	return err
}
